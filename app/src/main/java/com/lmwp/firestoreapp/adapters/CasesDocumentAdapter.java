package com.lmwp.firestoreapp.adapters;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.lmwp.firestoreapp.R;
import com.lmwp.firestoreapp.interfaces.AddDocumentInCase;
import com.lmwp.firestoreapp.models.CaseDocuments;


import java.util.ArrayList;

public class CasesDocumentAdapter extends BaseAdapter {

    Activity context;
    ArrayList<CaseDocuments> workListDetails;
    AddDocumentInCase addDocumentInCase;

    public CasesDocumentAdapter(Activity context, ArrayList<CaseDocuments> modelList ) {
        this.context = context;
        this.workListDetails = modelList;
        this.addDocumentInCase = addDocumentInCase;
    }

    @Override
    public int getCount() {
        return workListDetails.size();
    }

    @Override
    public Object getItem(int position) {
        return workListDetails.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        convertView = null;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.documentlist_row, parent, false);
            TextView documentTitle = (TextView) convertView.findViewById(R.id.documentitle);
            TextView updatedDate = (TextView) convertView.findViewById(R.id.update_date);
            TextView documentLink = (TextView) convertView.findViewById(R.id.documentLink);

            final CaseDocuments item = workListDetails.get(position);
            Log.e("DueDate",""+item.getDocumentLink());
            documentTitle.setText(item.getTitle());
            updatedDate.setText(item.getUploaded());
            documentLink.setText(item.getDocumentLink());
        }
        return convertView;
    }



}