package com.lmwp.firestoreapp.adapters;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.lmwp.firestoreapp.R;
import com.lmwp.firestoreapp.interfaces.AddDocumentInCase;
import com.lmwp.firestoreapp.interfaces.changeWorkStatus;
import com.lmwp.firestoreapp.models.CaseList;


import java.util.ArrayList;

public class CasesAdapter extends BaseAdapter {

    Activity context;
    ArrayList<CaseList> workListDetails;
    AddDocumentInCase addDocumentInCase;

    public CasesAdapter(Activity context, ArrayList<CaseList> modelList, AddDocumentInCase addDocumentInCase ) {
        this.context = context;
        this.workListDetails = modelList;
        this.addDocumentInCase = addDocumentInCase;
    }

    @Override
    public int getCount() {
        return workListDetails.size();
    }

    @Override
    public Object getItem(int position) {
        return workListDetails.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        convertView = null;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.caselist_row, parent, false);
            TextView openDate = (TextView) convertView.findViewById(R.id.openDate);
            TextView close_date = (TextView) convertView.findViewById(R.id.close_date);
            TextView referanceId = (TextView) convertView.findViewById(R.id.referanceId);
            TextView statusText = (TextView) convertView.findViewById(R.id.statusText);
            TextView cumulativeText = (TextView) convertView.findViewById(R.id.cumulativeText);

            Button billing = (Button) convertView.findViewById(R.id.billing);
            Button addDocumentBtn = (Button) convertView.findViewById(R.id.addDocuments);
            Button viewDocumentBtn = (Button) convertView.findViewById(R.id.viewDocument);
            Button changeStatusBtn = (Button) convertView.findViewById(R.id.changeStatus);

            final CaseList item = workListDetails.get(position);

            //if(item.getCumulativeAmount()>=0)
            if((item.getCumulativeAmount()) >= 0)
            {
                cumulativeText.setTextColor(ContextCompat.getColor(context, R.color.green));
            }else{
                cumulativeText.setTextColor(ContextCompat.getColor(context, R.color.red));
            }
            cumulativeText.setText("Cumulative Amount "+(item.getCumulativeAmount()));
            referanceId.setText(item.getReferenceId());
            openDate.setText(item.getOpenedDate());
            close_date.setText(item.getClosedDate());
            statusText.setText("Status: "+item.getStatus());

            addDocumentBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    addDocumentInCase.addDocument(item.getReferenceId(),false);
                }
            });
            viewDocumentBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    addDocumentInCase.addDocument(item.getReferenceId(),true);
                }
            });
            billing.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    addDocumentInCase.addBilling(item);

                }
            });
            changeStatusBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    addDocumentInCase.changeStatus(item);
                }
            });
        }
        return convertView;
    }



}