package com.lmwp.firestoreapp.adapters;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.lmwp.firestoreapp.R;
import com.lmwp.firestoreapp.interfaces.viewClientDetails;
import com.lmwp.firestoreapp.models.ClientList;


import java.util.ArrayList;

public class ClientsAdapter extends BaseAdapter {

    Activity context;
    ArrayList<ClientList> clientLists;
    viewClientDetails viewclientdetails;

    public ClientsAdapter(Activity context, ArrayList<ClientList> modelList , viewClientDetails viewclientdetails) {
        this.context = context;
        this.clientLists = modelList;
        this.viewclientdetails = viewclientdetails;
    }

    @Override
    public int getCount() {
        return clientLists.size();
    }

    @Override
    public Object getItem(int position) {
        return clientLists.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        convertView = null;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.clientlist_row, parent, false);
            TextView fullName = (TextView) convertView.findViewById(R.id.fullName);
            TextView idNumber = (TextView) convertView.findViewById(R.id.idNumber);
            TextView contactnumber = (TextView) convertView.findViewById(R.id.Contactnumber);
            TextView firstconsultationdate = (TextView) convertView.findViewById(R.id.firstconsultationdate);

            Button viewClientDetails = (Button) convertView.findViewById(R.id.viewClientDetails);
            Button viewUpdateDetails = (Button) convertView.findViewById(R.id.updateDetails);

            final ClientList item = clientLists.get(position);

            fullName.setText(item.getFullname());
            idNumber.setText(item.getIdnumber());
            contactnumber.setText(item.getContactnumber1());
            //created_date.setText("Datetime: "+item.getCreateddate());
            firstconsultationdate.setText("FC Date: "+item.getFirstconsultationdate().replace("-","/"));

            viewClientDetails.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    viewclientdetails.checkDetails(item);
                }
            });
            viewUpdateDetails.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    viewclientdetails.updateDetails(item);
                }
            });
        }
        return convertView;
    }



}