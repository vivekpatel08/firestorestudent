package com.lmwp.firestoreapp.adapters;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import androidx.core.app.ActivityCompat;


import com.lmwp.firestoreapp.R;
import com.lmwp.firestoreapp.interfaces.changeWorkStatus;
import com.lmwp.firestoreapp.models.LPMAWorkList;

import java.util.ArrayList;

public class WorkListAdapter extends BaseAdapter {

    Activity context;
    ArrayList<LPMAWorkList> workListDetails;
    changeWorkStatus changeworkStatus;

    public WorkListAdapter(Activity context, ArrayList<LPMAWorkList> modelList, changeWorkStatus changeworkStatus ) {
        this.context = context;
        this.workListDetails = modelList;
        this.changeworkStatus = changeworkStatus;
    }

    @Override
    public int getCount() {
        return workListDetails.size();
    }

    @Override
    public Object getItem(int position) {
        return workListDetails.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        convertView = null;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.worklist_row, parent, false);
            TextView workTitle = (TextView) convertView.findViewById(R.id.worktitle);
            TextView due_date = (TextView) convertView.findViewById(R.id.due_date);
            TextView priority = (TextView) convertView.findViewById(R.id.priority);
            TextView created_date = (TextView) convertView.findViewById(R.id.created_date);
            TextView status = (TextView) convertView.findViewById(R.id.status);
            Button viewMoreDetails = (Button) convertView.findViewById(R.id.viewWorkDetails);
            TextView workdescription =(TextView) convertView.findViewById(R.id.workdescription);
            final LPMAWorkList item = workListDetails.get(position);

            workTitle.setText(item.getTitle());
            due_date.setText(item.getDueData());
            priority.setText("Priority: "+item.getPriority());
            //created_date.setText("Datetime: "+item.getCreateddate());
            created_date.setText("Status: "+item.getStatus());
            status.setText("");
            workdescription.setText(item.getDescription());
            viewMoreDetails.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    changeworkStatus.change(item);
                }
            });

        }
        return convertView;
    }



}