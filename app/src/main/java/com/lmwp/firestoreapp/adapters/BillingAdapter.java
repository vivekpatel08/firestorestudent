package com.lmwp.firestoreapp.adapters;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.lmwp.firestoreapp.R;
import com.lmwp.firestoreapp.models.BillingList;

import java.util.ArrayList;

public class BillingAdapter extends BaseAdapter {

    Activity context;
    ArrayList<BillingList> billingLists;


    public BillingAdapter(Activity context, ArrayList<BillingList> modelList) {
        this.context = context;
        this.billingLists = modelList;

    }

    @Override
    public int getCount() {
        return billingLists.size();
    }

    @Override
    public Object getItem(int position) {
        return billingLists.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        convertView = null;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.billinglist_row, parent, false);
            TextView title = (TextView) convertView.findViewById(R.id.title);
            TextView feeAmount = (TextView) convertView.findViewById(R.id.feeAmount);
            TextView description = (TextView) convertView.findViewById(R.id.description);
            TextView dateoftask = (TextView) convertView.findViewById(R.id.dateoftask);
            TextView debit_credit = (TextView) convertView.findViewById(R.id.debit_credit);
            final BillingList billingItem = billingLists.get(position);
            title.setText(billingItem.getTitle());
            feeAmount.setText(billingItem.getFee_amount());
            description.setText(billingItem.getDescription());
            dateoftask.setText(billingItem.getDateoftask());
            debit_credit.setText(billingItem.getDebit_credit());
        }
        return convertView;
    }



}