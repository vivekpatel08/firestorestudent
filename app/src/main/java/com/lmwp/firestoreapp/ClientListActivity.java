package com.lmwp.firestoreapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import com.lmwp.firestoreapp.adapters.ClientsAdapter;
import com.lmwp.firestoreapp.interfaces.viewClientDetails;
import com.lmwp.firestoreapp.models.ClientList;
import com.lmwp.firestoreapp.utils.DataBridge;


import java.util.ArrayList;
import java.util.List;

public class ClientListActivity extends AppCompatActivity {

    FloatingActionButton mAddFab, mAddWorkFab, mAddClientFab;
    TextView addAlarmActionText, addPersonActionText;
    Boolean isAllFabsVisible;
    ListView workListView;
    private FirebaseFirestore db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client_list);
        getSupportActionBar().setTitle("Clients List");
        ActionBar actionBar = getSupportActionBar();
        // showing the back button in action bar
        actionBar.setDisplayHomeAsUpEnabled(true);

        db = FirebaseFirestore.getInstance();
        mAddFab = findViewById(R.id.add_fab);
        workListView = findViewById(R.id.workList);

        mAddWorkFab = findViewById(R.id.add_alarm_fab);
        mAddClientFab = findViewById(R.id.add_person_fab);


        addAlarmActionText = findViewById(R.id.add_alarm_action_text);
        addPersonActionText = findViewById(R.id.add_person_action_text);
        mAddClientFab.setVisibility(View.GONE);
        mAddWorkFab.setVisibility(View.GONE);
        addAlarmActionText.setVisibility(View.GONE);
        addPersonActionText.setVisibility(View.GONE);

        isAllFabsVisible = false;
        mAddFab.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (!isAllFabsVisible) {


                           // mAddWorkFab.show();
                           mAddClientFab.show();
                         //  addAlarmActionText.setVisibility(View.VISIBLE);
                           addPersonActionText.setVisibility(View.VISIBLE);


                            isAllFabsVisible = true;
                        } else {


                           mAddClientFab.hide();
                            mAddWorkFab.hide();
                         //   addAlarmActionText.setVisibility(View.GONE);
                            addPersonActionText.setVisibility(View.GONE);


                            isAllFabsVisible = false;
                        }
                    }
                });


        mAddClientFab.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Intent i =new Intent(ClientListActivity.this,CreateClient.class);
                        startActivity(i);

                    }
                });

        mAddWorkFab.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (!isAllFabsVisible) {
                            mAddWorkFab.show();
                            mAddClientFab.show();
                            addAlarmActionText.setVisibility(View.VISIBLE);
                            addPersonActionText.setVisibility(View.VISIBLE);


                            isAllFabsVisible = true;
                        } else {


                            mAddClientFab.hide();
                            mAddWorkFab.hide();
                            addAlarmActionText.setVisibility(View.GONE);
                            addPersonActionText.setVisibility(View.GONE);


                            isAllFabsVisible = false;
                        }



                        Intent i=new Intent(ClientListActivity.this, CreateWork.class);
                        startActivity(i);
                    }
                });


        GetClientList();


    }

    private void GetClientList() {

        db.collection("clients")
                .get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                ArrayList<ClientList> clientLists = new ArrayList<>();
                List<DocumentSnapshot> list = queryDocumentSnapshots.getDocuments();
                for (DocumentSnapshot d : list) {

                    Log.d("WorkList", "DocumentSnapshot added with ID: " + d.getData());
                    ClientList dataModal = d.toObject(ClientList.class);
                    dataModal.setId(d.getId());
                    clientLists.add(dataModal);
                }

                ClientsAdapter adaptor = new ClientsAdapter(ClientListActivity.this, clientLists, new viewClientDetails() {
                    @Override
                    public void checkDetails(ClientList clientId) {
                        DataBridge.selectedClientDetails =clientId;

                        Intent i=new Intent(ClientListActivity.this,CasesListActivity.class);
                        startActivity(i);

                    }

                    @Override
                    public void updateDetails(ClientList clientdetails) {
                        DataBridge.selectedClientDetails =clientdetails;

                        Intent i=new Intent(ClientListActivity.this,UpdateClientDetails.class);
                        startActivity(i);

                    }
                });
                //set the view for the Drop down list
                //set the ArrayAdapter to the spinner
                workListView.setAdapter(adaptor);
            }
        });
    }

    @Override
    protected void onResume() {
        GetClientList();
        super.onResume();
    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}