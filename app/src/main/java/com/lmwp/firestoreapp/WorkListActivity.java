package com.lmwp.firestoreapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.lmwp.firestoreapp.adapters.WorkListAdapter;
import com.lmwp.firestoreapp.interfaces.changeWorkStatus;
import com.lmwp.firestoreapp.models.LPMAWorkList;
import com.lmwp.firestoreapp.utils.SessionManage;

import java.util.ArrayList;
import java.util.List;

public class WorkListActivity extends AppCompatActivity {

    FloatingActionButton mAddFab, mAddWorkFab, mAddClientFab,mLogout;

    TextView addAlarmActionText, addPersonActionText,logoutText;
    Boolean isAllFabsVisible;
    ListView workListView;
    private FirebaseFirestore db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_work_list);
        getSupportActionBar().setTitle("Work List");
        SessionManage obj=new SessionManage(WorkListActivity.this);


        if(obj.PickEmpId(SessionManage.UserId)==0)
        {
            Intent i=new Intent(WorkListActivity.this,LoginActivity.class);
            startActivity(i);
        }


        //ActionBar actionBar = getSupportActionBar();
        //actionBar.setDisplayHomeAsUpEnabled(true);

        db = FirebaseFirestore.getInstance();
        mAddFab = findViewById(R.id.add_fab);
        workListView = findViewById(R.id.workList);

        mAddWorkFab = findViewById(R.id.add_alarm_fab);
        mAddClientFab = findViewById(R.id.add_person_fab);
        mLogout = findViewById(R.id.logout);


        addAlarmActionText = findViewById(R.id.add_alarm_action_text);
        logoutText = findViewById(R.id.logoutText);
        addPersonActionText = findViewById(R.id.add_person_action_text);
        mAddClientFab.setVisibility(View.GONE);
        mLogout.setVisibility(View.GONE);
        mAddWorkFab.setVisibility(View.GONE);
        addAlarmActionText.setVisibility(View.GONE);
        addPersonActionText.setVisibility(View.GONE);
        logoutText.setVisibility(View.GONE);

        isAllFabsVisible = false;
        mAddFab.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (!isAllFabsVisible) {


                            mAddWorkFab.show();
                            mAddClientFab.show();
                            mLogout.show();
                            addAlarmActionText.setVisibility(View.VISIBLE);
                            addPersonActionText.setVisibility(View.VISIBLE);
                            logoutText.setVisibility(View.VISIBLE);


                            isAllFabsVisible = true;
                        } else {


                            mAddClientFab.hide();
                            mAddWorkFab.hide();
                            mLogout.hide();
                            addAlarmActionText.setVisibility(View.GONE);
                            addPersonActionText.setVisibility(View.GONE);
                            logoutText.setVisibility(View.GONE);


                            isAllFabsVisible = false;
                        }
                    }
                });


        mAddClientFab.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Intent i =new Intent(WorkListActivity.this,ClientListActivity.class);
                        startActivity(i);

                    }
                });

        mAddWorkFab.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (!isAllFabsVisible) {
                            mAddWorkFab.show();
                            mAddClientFab.show();
                            mLogout.show();
                            addAlarmActionText.setVisibility(View.VISIBLE);
                            addPersonActionText.setVisibility(View.VISIBLE);
                            logoutText.setVisibility(View.VISIBLE);

                            isAllFabsVisible = true;
                        } else {


                            mAddClientFab.hide();
                            mAddWorkFab.hide();
                            mLogout.hide();
                            addAlarmActionText.setVisibility(View.GONE);
                            addPersonActionText.setVisibility(View.GONE);
                            logoutText.setVisibility(View.GONE);

                            isAllFabsVisible = false;
                        }
                        Intent i=new Intent(WorkListActivity.this, CreateWork.class);
                        startActivity(i);
                    }
                });

        mLogout.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //SessionManage obj=new SessionManage(WorkListActivity.this);
                       // obj.RemoveAllDataInSesssion();
                        SessionManage obj=new SessionManage(WorkListActivity.this);
                        obj.SessionStore(0,  "Ecode" ,  "Eposition",  "Ename",  "zoneId");
                        Intent i=new Intent(WorkListActivity.this, LoginActivity.class);
                        startActivity(i);
                    }
                });


        GetWorkList();


    }

    private void GetWorkList() {

        db.collection("work")
                .orderBy("autoIncrementId", Query.Direction.ASCENDING)
                .get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                ArrayList<LPMAWorkList> workList = new ArrayList<>();
                List<DocumentSnapshot> list = queryDocumentSnapshots.getDocuments();
                for (DocumentSnapshot d : list) {

                    Log.d("WorkList", "DocumentSnapshot added with ID: " + d.getData());
                    LPMAWorkList dataModal = d.toObject(LPMAWorkList.class);
                    dataModal.setId(d.getId());
                    workList.add(dataModal);
                }

                WorkListAdapter adaptor = new WorkListAdapter(WorkListActivity.this, workList, new changeWorkStatus() {
                    @Override
                    public void change(LPMAWorkList workItemData) {

ViewDialog obj=new ViewDialog();
obj.showDialogDocumentList(WorkListActivity.this,workItemData);

                    }
                });
                //set the view for the Drop down list
                //set the ArrayAdapter to the spinner
                workListView.setAdapter(adaptor);
            }
        });
    }
    public class ViewDialog {




        public void showDialogDocumentList(Activity activity, LPMAWorkList workItemData) {

            final Dialog dialog = new Dialog(activity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.dialog_change_status);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            Button mDialogBtnSaveStatus = dialog.findViewById(R.id.idSaveStatus);
            Button mDialogCancel = dialog.findViewById(R.id.idcancelDialog);
            Spinner mDialogStatusSpinner = dialog.findViewById(R.id.statusSpinner);

            ArrayList<String> designationListName = new ArrayList<String>();
            designationListName.add("Open");
            designationListName.add("In-Progress");
            designationListName.add("Pending");
            designationListName.add("Closed");
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(WorkListActivity.this, R.layout.row_spn, designationListName);
            adapter.setDropDownViewResource(R.layout.row_spn_dropdown);
            mDialogStatusSpinner.setAdapter(adapter);
            mDialogStatusSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                    workItemData.setStatus(designationListName.get(position).toString());

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
            mDialogCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.cancel();
                }
            });

            mDialogBtnSaveStatus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    dialog.cancel();
                    updateWorkStatus(workItemData);

                }
            });
            dialog.show();

        }
    }

    private void updateWorkStatus(LPMAWorkList workItemData) {
        Log.e("Id",":"+workItemData.getId());
        db.collection("work").
                // below line is use toset the id of
                // document where we have to perform
                // update operation.

                document(workItemData.getId()).

                // after setting our document id we are
                // passing our whole object class to it.
                        set(workItemData).

                // after passing our object class we are
                // calling a method for on success listener.
                        addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        // on successful completion of this process
                        // we are displaying the toast message.
                        Toast.makeText(WorkListActivity.this, "Work has been updated..", Toast.LENGTH_SHORT).show();
                        GetWorkList();
                    }
                }).addOnFailureListener(new OnFailureListener() {
            // inside on failure method we are
            // displaying a failure message.
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(WorkListActivity.this, "Fail to update the data,please try later.", Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    protected void onResume() {
        GetWorkList();
        super.onResume();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finishAffinity();
        super.onBackPressed();
    }
}