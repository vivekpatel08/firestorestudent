package com.lmwp.firestoreapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.lmwp.firestoreapp.utils.SessionManage;

public class LoginOrRegister extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_login_or_register);

        SessionManage obj=new SessionManage(LoginOrRegister.this);


        if(obj.PickEmpId(SessionManage.UserId)!=0)
        {
            Intent i=new Intent(LoginOrRegister.this,WorkListActivity.class);
            startActivity(i);
        }

        findViewById(R.id.idLogin).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i =new Intent(LoginOrRegister.this,LoginActivity.class);
                startActivity(i);
            }
        });
        findViewById(R.id.idRegister).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i =new Intent(LoginOrRegister.this,MainActivity.class);
                startActivity(i);
            }
        });
    }
}