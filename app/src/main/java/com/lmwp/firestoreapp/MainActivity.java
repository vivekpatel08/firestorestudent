package com.lmwp.firestoreapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {


    private static final String TAG = "MainAc";

    private EditText userNameEdt, emailAddressEdt, passwordEdt,mobileNumberEdt,fullnameEdt;
    private Button submitSaveBtn;
    private String username, password, emailAdress,mobileNumber,fullname;
    private FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().setTitle("Register");
        ActionBar actionBar = getSupportActionBar();

        // showing the back button in action bar
        actionBar.setDisplayHomeAsUpEnabled(true);

        // initializing our edittext and buttons
        userNameEdt = findViewById(R.id.idEdtUsername);
        fullnameEdt = findViewById(R.id.idEdtfullname);
        passwordEdt = findViewById(R.id.idEdtPassword);
        emailAddressEdt = findViewById(R.id.idEdtEmailAddress);
        mobileNumberEdt = findViewById(R.id.idEdtMobileNumber);
        submitSaveBtn = findViewById(R.id.idSave);
        db = FirebaseFirestore.getInstance();
        // adding on click listener for button
        submitSaveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getSupportActionBar().setTitle("Register");
                // getting data from edittext fields.
                username = userNameEdt.getText().toString();
                password = passwordEdt.getText().toString();
                emailAdress = emailAddressEdt.getText().toString();
                mobileNumber = mobileNumberEdt.getText().toString();
                fullname = fullnameEdt.getText().toString();

                // validating the text fields if empty or not.

                if (TextUtils.isEmpty(fullname)) {
                    fullnameEdt.setError("Please enter full name");
                } else if (TextUtils.isEmpty(username)) {
                    userNameEdt.setError("Please enter Username");
                } else if (TextUtils.isEmpty(emailAdress)) {
                    emailAddressEdt.setError("Please enter email address");
                } else if (TextUtils.isEmpty(password)) {
                    passwordEdt.setError("Please enter password");

                } else if(TextUtils.isEmpty(password)) {
                    mobileNumberEdt.setError("Please enter mobile number");

                }else{

                    db.collection("users")
                            .whereEqualTo("username",username)
                            .get()
                            .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                @Override
                                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                    if(task.getResult().isEmpty())
                                    {
                                        db.collection("users")
                                                .whereEqualTo("email",emailAdress)
                                                .get()
                                                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                                        if(task.getResult().isEmpty())
                                                        {
                                                            db.collection("users")
                                                                    .whereEqualTo("phone",mobileNumber)
                                                                    .get()
                                                                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                                                        @Override
                                                                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                                                            if(task.getResult().isEmpty())
                                                                            {
                                                                                addDataToFirestore(username, password, emailAdress,mobileNumber,fullname);
                                                                            }else{
                                                                                Toast.makeText(MainActivity.this, "Phone number already registered", Toast.LENGTH_SHORT).show();
                                                                            }

                                                                            if (task.isSuccessful()) {
                                                                                for (QueryDocumentSnapshot document : task.getResult()) {
                                                                                    Log.e(TAG, "Data Checking"+document.getId() + " => " + document.getData());
                                                                                }

                                                                            } else {
                                                                                Log.e(TAG, "Error getting documents.", task.getException());
                                                                            }
                                                                        }
                                                                    });

                                                        }else{
                                                            Toast.makeText(MainActivity.this, "Email Address already registered", Toast.LENGTH_SHORT).show();
                                                        }

                                                        if (task.isSuccessful()) {
                                                            for (QueryDocumentSnapshot document : task.getResult()) {
                                                                Log.e(TAG, "Data Checking"+document.getId() + " => " + document.getData());
                                                            }

                                                        } else {
                                                            Log.e(TAG, "Error getting documents.", task.getException());
                                                        }
                                                    }
                                                });
                                    }else{
                                        Toast.makeText(MainActivity.this, "Username already registered", Toast.LENGTH_SHORT).show();
                                    }

                                    if (task.isSuccessful()) {
                                        for (QueryDocumentSnapshot document : task.getResult()) {
                                            Log.e(TAG, "Data Checking"+document.getId() + " => " + document.getData());
                                        }

                                    } else {
                                        Log.e(TAG, "Error getting documents.", task.getException());
                                    }
                                }
                            });







                }
            }
        });

        db.collection("users")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Log.e(TAG, document.getId() + " => " + document.getData());
                            }
                        } else {
                            Log.e(TAG, "Error getting documents.", task.getException());
                        }
                    }
                });


    }



    private void addDataToFirestore(String username, String password, String emailaddress, String mobilenumber,String fullname) {
        Map<String, Object> user ;
        db = FirebaseFirestore.getInstance();
        // Create a new user with a first and last name
        user = new HashMap<>();
        user.put("fullname", fullname);
        user.put("username", username);
        user.put("password", password);
        user.put("email", emailaddress);
        user.put("phone", mobilenumber);
        db.collection("users")
                .add(user)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Toast.makeText(MainActivity.this, "Registration done successfully", Toast.LENGTH_SHORT).show();
                        onBackPressed();
                        Log.d(TAG, "DocumentSnapshot added with ID: " + documentReference.getId());
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error adding document", e);
                    }
                });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}