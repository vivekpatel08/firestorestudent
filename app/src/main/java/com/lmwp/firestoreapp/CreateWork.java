package com.lmwp.firestoreapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.lmwp.firestoreapp.utils.DatePickerFragment;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

public class CreateWork extends AppCompatActivity {


    private static final String TAG = "MainAc";
    RadioButton highRadio, lowRadio,mediumRadio,highestRadio;
    private EditText titleEdt, descriptionEdt,dueDateEdit;
    ImageView dueDateEdt;
    private Button submitWorkBtn;
    private String title, description, dueDate,priority="Low";
    private FirebaseFirestore db;
    DatePickerDialog.OnDateSetListener ondate;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_work_list);

        getSupportActionBar().setTitle("Create work");
        ActionBar actionBar = getSupportActionBar();

        // showing the back button in action bar
        actionBar.setDisplayHomeAsUpEnabled(true);

        // initializing our edittext and buttons
        titleEdt = findViewById(R.id.idEdtTitle);
        descriptionEdt = findViewById(R.id.idEdtDescription);
        dueDateEdt = findViewById(R.id.idEdtDueDate);
         lowRadio = findViewById(R.id.low);
         mediumRadio = findViewById(R.id.medium);
         highRadio = findViewById(R.id.high);
         highestRadio = findViewById(R.id.highest);
        dueDateEdit = findViewById(R.id.dueDateEdit);

        submitWorkBtn = findViewById(R.id.idSaveWork);
        db = FirebaseFirestore.getInstance();

        lowRadio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                priority="Low";
            }
        });
        highRadio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                priority="High";
            }
        });
        highestRadio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                priority="Highest";
            }
        });
        mediumRadio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                priority="Medium";
            }
        });


        // adding on click listener for button
        submitWorkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // getting data from edittext fields.
                title = titleEdt.getText().toString();
                description = descriptionEdt.getText().toString();
                //dueDate = dueDateEdt.getText().toString();



                // validating the text fields if empty or not.

                if (TextUtils.isEmpty(title)) {
                    titleEdt.setError("Please enter title");
                } else if (TextUtils.isEmpty(description)) {
                    descriptionEdt.setError("Please enter description");
                } else if (TextUtils.isEmpty(dueDate)) {
                    dueDateEdit.setError("Please select due date.");
                }  else{

               saveWorkLDetails();



                }
            }
        });
        dueDateEdt.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("NewApi")
            @Override
            public void onClick(View v) {

                showDatePicker();

            }
        });
        ondate = new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {

                dueDateEdit.setText(String.valueOf(dayOfMonth) + "-" + String.valueOf(monthOfYear + 1)
                        + "-" + String.valueOf(year));


                dueDate=String.valueOf(year) + "-" + String.valueOf(monthOfYear + 1)
                        + "-" + String.valueOf(dayOfMonth);
            }
        };

    }
    private void showDatePicker() {
        DatePickerFragment date = new DatePickerFragment();
        /**
         * Set Up Current Date Into dialog
         */
        Calendar calender = Calendar.getInstance();
        Bundle args = new Bundle();
        args.putInt("year", calender.get(Calendar.YEAR));
        args.putInt("month", calender.get(Calendar.MONTH));
        args.putInt("day", calender.get(Calendar.DAY_OF_MONTH));
        date.setArguments(args);
        /**
         * Set Call back to capture selected date
         */
        date.setCallBack(ondate);
        date.show( getSupportFragmentManager(),"Date Picker");
    }



    private void saveWorkLDetails() {

        Map<String, Object> user ;
        db = FirebaseFirestore.getInstance();
        // Create a new user with a first and last name
        Date date = new Date();
        user = new HashMap<>();

        user.put("title", title);
        user.put("description", description);

        user.put("priority", priority);

        Date dateData = Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

        String strDate = dateFormat.format(dateData);
        DateFormat dateFormat1 = new SimpleDateFormat("ddMMyyyyHHmmss");

        String strDate1 = dateFormat1.format(dateData);

        user.put("autoIncrementId", strDate1);
        user.put("createddate", strDate);
        user.put("status", "In-Progress");

        int randumNumber = ThreadLocalRandom.current().nextInt();
        user.put("status", "In-Progress");
        user.put("workId", "W"+randumNumber);
        user.put("dueData", dueDate);
        //user.put("createddate", Clock.systemUTC().instant());

        db.collection("work")
                .add(user)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Toast.makeText(CreateWork.this, "Work details saved successfully.", Toast.LENGTH_SHORT).show();
                        onBackPressed();
                        Log.d(TAG, "DocumentSnapshot added with ID: " + documentReference.getId());
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error adding document", e);
                        Toast.makeText(CreateWork.this, "Failed to save details, please try later.", Toast.LENGTH_SHORT).show();
                    }
                });

    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}