package com.lmwp.firestoreapp;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import com.lmwp.firestoreapp.adapters.CasesAdapter;
import com.lmwp.firestoreapp.adapters.CasesDocumentAdapter;
import com.lmwp.firestoreapp.interfaces.AddDocumentInCase;
import com.lmwp.firestoreapp.models.BillingList;
import com.lmwp.firestoreapp.models.CaseDocuments;
import com.lmwp.firestoreapp.models.CaseList;
import com.lmwp.firestoreapp.utils.DataBridge;
import com.lmwp.firestoreapp.utils.DatePickerFragment;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CasesListActivity extends AppCompatActivity {
    String TitleString, UploadedString, DocumentLinkString, CaseIdString;
    FloatingActionButton mAddFab, mAddWorkFab, mAddClientFab;
    TextView addAlarmActionText, addPersonActionText,fullNameText,physicaladdressText,cumulativeAmountText;
    Boolean isAllFabsVisible;
    ListView caseListView;
    ListView  documentListView;
    DatePickerDialog.OnDateSetListener ondate;
    private FirebaseFirestore db;
    ArrayList<CaseList> caseLists;
    CasesAdapter adaptor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_case_list);
        getSupportActionBar().setTitle("Client Details");

        ActionBar actionBar = getSupportActionBar();
        // showing the back button in action bar
        actionBar.setDisplayHomeAsUpEnabled(true);

        db = FirebaseFirestore.getInstance();
        mAddFab = findViewById(R.id.add_fab);
        fullNameText = findViewById(R.id.fullnameclient);
        physicaladdressText = findViewById(R.id.physicaladdressClient);
        cumulativeAmountText = findViewById(R.id.cumulativeAmount);
        fullNameText.setText(DataBridge.selectedClientDetails.getFullname());
        physicaladdressText.setText(DataBridge.selectedClientDetails.getPhysicaladdress());
        caseListView = findViewById(R.id.caseList);
        mAddWorkFab = findViewById(R.id.add_alarm_fab);
        mAddClientFab = findViewById(R.id.add_person_fab);
        addAlarmActionText = findViewById(R.id.add_alarm_action_text);
        addPersonActionText = findViewById(R.id.add_person_action_text);
        mAddClientFab.setVisibility(View.GONE);
        mAddWorkFab.setVisibility(View.GONE);
        addAlarmActionText.setVisibility(View.GONE);
        addPersonActionText.setVisibility(View.GONE);
        isAllFabsVisible = false;
        mAddFab.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (!isAllFabsVisible) {
                            //mAddWorkFab.show();
                            mAddClientFab.show();
                           // addAlarmActionText.setVisibility(View.VISIBLE);
                            addPersonActionText.setVisibility(View.VISIBLE);
                            isAllFabsVisible = true;
                        } else {
                            mAddClientFab.hide();
                          //  mAddWorkFab.hide();
                          //  addAlarmActionText.setVisibility(View.GONE);
                            addPersonActionText.setVisibility(View.GONE);
                            isAllFabsVisible = false;
                        }
                    }
                });
        mAddClientFab.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i =new Intent(CasesListActivity.this,CreateCases.class);
                        startActivity(i);
                    }
                });

    }

    private void GetCaseList() {

        db.collection("cases").whereEqualTo("idnumber",DataBridge.selectedClientDetails.getIdnumber())
                .get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                caseLists = new ArrayList<>();
                List<DocumentSnapshot> list = queryDocumentSnapshots.getDocuments();
                for (DocumentSnapshot d : list) {
                    Log.d("Cases", "DocumentSnapshot added with ID: " + d.getData());
                    CaseList dataModal = d.toObject(CaseList.class);
                    dataModal.setId(d.getId());
                    caseLists.add(dataModal);
                }

               adaptor = new CasesAdapter(CasesListActivity.this, caseLists, new AddDocumentInCase() {
                    @Override
                    public void addDocument(String caseReferenceId,boolean isViewDocument) {
                        CaseIdString=caseReferenceId;
                        ViewDialog obj=new ViewDialog();
                        if(isViewDocument)
                        {
                            obj.showDialogDocumentList(CasesListActivity.this);
                        }else{

                            obj.showDialogToCreateDocument(CasesListActivity.this);
                        }
                    }

                    @Override
                    public void addBilling(CaseList item) {
                        DataBridge.selectedCaseItem=item;
                        Intent i =new Intent(CasesListActivity.this,BillingListView.class);
                        startActivity(i);
                    }

                    @Override
                    public void changeStatus(CaseList itemData) {
                        ViewDialog obj=new ViewDialog();
                        obj.showDialogStatusList(CasesListActivity.this,itemData);
                    }
                });
                //set the view for the Drop down list
                //set the ArrayAdapter to the spinner
                caseListView.setAdapter(adaptor);

                CalculateCumulative(caseLists);
            }
        });



    }

    private void CalculateCumulative(ArrayList<CaseList> caseLists) {
        if(caseLists.size()==0)
        {
            return;
        }
        ArrayList<BillingList> billingLists = new ArrayList<>();
        Log.e("Calling ","Billing Data");
        List<String> values = new ArrayList<>();
        for (int i = 0; i < caseLists.size(); i++) {
            values.add(caseLists.get(i).getReferenceId());
        }

        db.collection("billing").whereIn("referanceId",values)
                .get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {

                List<DocumentSnapshot> list = queryDocumentSnapshots.getDocuments();
                for (DocumentSnapshot d : list) {
                    Log.e("Billings", " " + d.getData());
                    BillingList dataModal = d.toObject(BillingList.class);
                    dataModal.setId(d.getId());
                    billingLists.add(dataModal);
                }
                    Log.e("Running","checkCumulative");
                    checkCumulative(billingLists);
            }


        });



        for (int i = 0; i < caseLists.size(); i++) {
            CaseList caseList=caseLists.get(i);
            Log.e("Calling ",":"+caseList.getReferenceId());
            int finalI = i;

            /*db.collection("billing").whereEqualTo("referanceId",caseList.getReferenceId())
                    .get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                @Override
                public void onSuccess(QuerySnapshot queryDocumentSnapshots) {

                    List<DocumentSnapshot> list = queryDocumentSnapshots.getDocuments();
                    for (DocumentSnapshot d : list) {
                        Log.e("Billings", " " + d.getData());
                        BillingList dataModal = d.toObject(BillingList.class);
                        dataModal.setId(d.getId());
                        billingLists.add(dataModal);
                    }

                    if( finalI == (caseLists.size()-1)){
                       Log.e("Running","checkCumulative");
                        checkCumulative(billingLists);
                    }
                }


            });*/
        }

       /* for (BillingList billList: billingLists ) {

            Log.e("BillingListData",""+billList.getId()+""+billList.getReferanceId()+""+billList.getDateoftask());
        }*/





    }

    private void checkCumulative(ArrayList<BillingList> billingLists) {
        for (int i = 0; i < caseLists.size(); i++) {
            CaseList caseItem=caseLists.get(i);
            for (int j = 0; j < billingLists.size(); j++) {
                BillingList billingItem=billingLists.get(j);
                if(caseItem.getReferenceId().equalsIgnoreCase(billingItem.getReferanceId()))
                {
                    if(billingItem.getDebit_credit().equalsIgnoreCase("Credit")){
                        caseItem.setCumulativeAmount(Integer.parseInt(billingItem.getFee_amount())+caseItem.getCumulativeAmount());
                    }else{
                        caseItem.setCumulativeAmount(caseItem.getCumulativeAmount()-Integer.parseInt(billingItem.getFee_amount()));

                    }

                }
            }
        }
        int cumulativeTemp=0;
        for (int i = 0; i < caseLists.size(); i++) {
            Log.e("Cuu",""+caseLists.get(i).getCumulativeAmount());
            cumulativeTemp=cumulativeTemp+caseLists.get(i).getCumulativeAmount();
        }
        adaptor.notifyDataSetChanged();

        cumulativeAmountText.setText("Cumulative Amount:" + (20000+cumulativeTemp));

        if(((20000+cumulativeTemp)) >= 0)
        {
            cumulativeAmountText.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.green));
        }else{
            cumulativeAmountText.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.red));
        }

    }

    @Override
    protected void onResume() {
        GetCaseList();
        super.onResume();
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public class ViewDialog {

        public void showDialogToCreateDocument(Activity activity) {

            final Dialog dialog = new Dialog(activity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.dialog_create_document);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

            EditText idEdtTitle = dialog.findViewById(R.id.idEdtTitle);
            EditText idEdtUploaded = dialog.findViewById(R.id.idEdtUploaded);
            ImageView idEdtUploadedImage = dialog.findViewById(R.id.idEdtUploadedImage);
            EditText idEdtDocumentLink = dialog.findViewById(R.id.idEdtDocumentLink);
            Button mDialogNo = dialog.findViewById(R.id.idSaveDocument);
            mDialogNo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    TitleString=idEdtTitle.getText().toString();
                    UploadedString=idEdtUploaded.getText().toString();
                    DocumentLinkString=idEdtDocumentLink.getText().toString();

                    saveDocumentDetails(TitleString, UploadedString, DocumentLinkString, CaseIdString);


                    dialog.dismiss();
                }
            });

            Button mDialogCancel = dialog.findViewById(R.id.idcancelDialog);
            mDialogCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    dialog.cancel();
                }
            });
            idEdtUploadedImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showDatePicker();
                }
            });
            ondate = new DatePickerDialog.OnDateSetListener() {

                public void onDateSet(DatePicker view, int year, int monthOfYear,
                                      int dayOfMonth) {

                    idEdtUploaded.setText(String.valueOf(dayOfMonth) + "-" + String.valueOf(monthOfYear + 1)
                            + "-" + String.valueOf(year));
                }
            };
            dialog.show();
        }

        public void showDialogDocumentList(Activity activity) {

            final Dialog dialog = new Dialog(activity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.dialog_document_list);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            ImageView mDialogCancel = dialog.findViewById(R.id.cancelDialog);
            documentListView = dialog.findViewById(R.id.documentList);
            mDialogCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.cancel();
                }
            });
            dialog.show();
            GetDocumentList();
        }


        public void showDialogStatusList(Activity activity, CaseList caseItemData) {

            final Dialog dialog = new Dialog(activity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.dialog_change_status);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            Button mDialogBtnSaveStatus = dialog.findViewById(R.id.idSaveStatus);
            Button mDialogCancel = dialog.findViewById(R.id.idcancelDialog);
            Spinner mDialogStatusSpinner = dialog.findViewById(R.id.statusSpinner);

            ArrayList<String> statusListName = new ArrayList<String>();
            statusListName.add("Open");
            statusListName.add("In-Progress");
            statusListName.add("Pending");
            statusListName.add("Closed");
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(CasesListActivity.this, R.layout.row_spn, statusListName);
            adapter.setDropDownViewResource(R.layout.row_spn_dropdown);
            mDialogStatusSpinner.setAdapter(adapter);
            mDialogStatusSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                    caseItemData.setStatus(statusListName.get(position).toString());

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
            mDialogCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.cancel();
                }
            });

            mDialogBtnSaveStatus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    dialog.cancel();
                    updateCaseStatus(caseItemData);

                }
            });
            dialog.show();

        }
    }

    private void saveDocumentDetails(String Title, String Uploaded,String DocumentLink,String CaseId) {

        Map<String, Object> user ;
        db = FirebaseFirestore.getInstance();
        // Create a new user with a first and last name
        Date date = new Date();
        user = new HashMap<>();

        user.put("Title", Title);
        user.put("Uploaded", Uploaded);
        user.put("CaseId", CaseId);
        user.put("DocumentLink", DocumentLink);
        db.collection("documents")
                .add(user)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Toast.makeText(CasesListActivity.this, "Document details saved successfully.", Toast.LENGTH_SHORT).show();
                        Log.d("Document", "DocumentSnapshot added with ID: " + documentReference.getId());
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("TAG", "Error adding document", e);
                        Toast.makeText(CasesListActivity.this, "Failed to save details, please try later.", Toast.LENGTH_SHORT).show();
                    }
                });

    }

    private void GetDocumentList() {

        db.collection("documents").whereEqualTo("CaseId",CaseIdString)
                .get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                ArrayList<CaseDocuments> caseDocumentList = new ArrayList<>();
                List<DocumentSnapshot> list = queryDocumentSnapshots.getDocuments();
                for (DocumentSnapshot d : list) {
                    Log.d("Cases Documents", "DocumentSnapshot added with ID: " + d.getData());
                    CaseDocuments dataModal = d.toObject(CaseDocuments.class);
                    caseDocumentList.add(dataModal);
                }

                CasesDocumentAdapter adaptor = new CasesDocumentAdapter(CasesListActivity.this, caseDocumentList);
                //set the view for the Drop down list
                //set the ArrayAdapter to the spinner
                documentListView.setAdapter(adaptor);
            }
        });
    }
    private void showDatePicker() {
        DatePickerFragment date = new DatePickerFragment();
        /**
         * Set Up Current Date Into dialog
         */
        Calendar calender = Calendar.getInstance();
        Bundle args = new Bundle();
        args.putInt("year", calender.get(Calendar.YEAR));
        args.putInt("month", calender.get(Calendar.MONTH));
        args.putInt("day", calender.get(Calendar.DAY_OF_MONTH));
        date.setArguments(args);
        /**
         * Set Call back to capture selected date
         */
        date.setCallBack(ondate);
        date.show( getSupportFragmentManager(),"Date Picker");
    }


    private void updateCaseStatus(CaseList caseListData) {
        Log.e("Id",":"+caseListData.getId());
        Log.e("Other",":"+caseListData.getStatus());

        db.collection("cases").
                // below line is use toset the id of
                // document where we have to perform
                // update operation.

                        document(caseListData.getId()).

                // after setting our document id we are
                // passing our whole object class to it.
                        set(caseListData).

                // after passing our object class we are
                // calling a method for on success listener.
                        addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        // on successful completion of this process
                        // we are displaying the toast message.
                        Toast.makeText(CasesListActivity.this, "Case has been updated..", Toast.LENGTH_SHORT).show();
                        GetCaseList();
                    }
                }).addOnFailureListener(new OnFailureListener() {
            // inside on failure method we are
            // displaying a failure message.
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(CasesListActivity.this, "Fail to update the data,please try later.", Toast.LENGTH_SHORT).show();
            }
        });
    }
}