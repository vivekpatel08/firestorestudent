package com.lmwp.firestoreapp.models;

import com.google.firebase.firestore.Exclude;

import java.io.Serializable;

public class ClientList implements Serializable {
    @Exclude
    private String id;

   private String fullname;
    private String idnumber;
    private String dateofbirth;
    private String contactnumber1;
    private String contactnumber2;
    private String physicaladdress;
    private String firstconsultationdate;

    public ClientList(String fullname, String idnumber, String dateofbirth, String contactnumber1, String contactnumber2, String physicaladdress, String firstconsultationdate) {
        this.fullname = fullname;
        this.idnumber = idnumber;
        this.dateofbirth = dateofbirth;
        this.contactnumber1 = contactnumber1;
        this.contactnumber2 = contactnumber2;
        this.physicaladdress = physicaladdress;
        this.firstconsultationdate = firstconsultationdate;
    }

    public ClientList(){

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getIdnumber() {
        return idnumber;
    }

    public void setIdnumber(String idnumber) {
        this.idnumber = idnumber;
    }

    public String getDateofbirth() {
        return dateofbirth;
    }

    public void setDateofbirth(String dateofbirth) {
        this.dateofbirth = dateofbirth;
    }

    public String getContactnumber1() {
        return contactnumber1;
    }

    public void setContactnumber1(String contactnumber1) {
        this.contactnumber1 = contactnumber1;
    }

    public String getContactnumber2() {
        return contactnumber2;
    }

    public void setContactnumber2(String contactnumber2) {
        this.contactnumber2 = contactnumber2;
    }

    public String getPhysicaladdress() {
        return physicaladdress;
    }

    public void setPhysicaladdress(String physicaladdress) {
        this.physicaladdress = physicaladdress;
    }

    public String getFirstconsultationdate() {
        return firstconsultationdate;
    }

    public void setFirstconsultationdate(String firstconsultationdate) {
        this.firstconsultationdate = firstconsultationdate;
    }
}
