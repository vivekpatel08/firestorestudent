package com.lmwp.firestoreapp.models;

import com.google.firebase.firestore.Exclude;

import java.io.Serializable;

public class LPMAWorkList implements Serializable {
    @Exclude
    private String id;
    private String createddate;
    private String Title;
    private String description;
    private String dueData;
    private String Priority;
    private String Status;
    private String workId;

    public LPMAWorkList(String createddate, String title, String description, String dueData, String priority, String status,String workId) {
        this.createddate = createddate;
        this.Title = title;
        this.description = description;
        this.dueData = dueData;
        this.Priority = priority;
        this.Status = status;
        this.workId = workId;
    }
    public LPMAWorkList()
    {}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String Description) {
        description = Description;
    }

    public String getCreateddate() {
        return createddate;
    }

    public void setCreateddate(String createddate) {
        this.createddate = createddate;
    }

    public String getDueData() {
        return dueData;
    }

    public void setDueData(String dueData) {
        this.dueData = dueData;
    }

    public String getPriority() {
        return Priority;
    }

    public void setPriority(String priority) {
        Priority = priority;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getWorkId() {
        return workId;
    }

    public void setWorkId(String workId) {
        this.workId = workId;
    }
}
