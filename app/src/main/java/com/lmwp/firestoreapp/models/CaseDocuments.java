package com.lmwp.firestoreapp.models;

import com.google.firebase.firestore.Exclude;

import java.io.Serializable;

public class CaseDocuments implements Serializable {
    String Title;
    String Uploaded;
    String DocumentLink;
    String CaseId;
    @Exclude
    private String id;
    public CaseDocuments()
    {

    }
    public CaseDocuments(String title, String uploaded, String documentLink,String CaseId) {
        this.Title = title;
        this.Uploaded = uploaded;
        this.DocumentLink = documentLink;
        this.CaseId=CaseId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCaseId() {
        return CaseId;
    }

    public void setCaseId(String caseId) {
        CaseId = caseId;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getUploaded() {
        return Uploaded;
    }

    public void setUploaded(String uploaded) {
        Uploaded = uploaded;
    }

    public String getDocumentLink() {
        return DocumentLink;
    }

    public void setDocumentLink(String documentLink) {
        DocumentLink = documentLink;
    }
}
