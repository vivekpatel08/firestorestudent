package com.lmwp.firestoreapp.models;

import com.google.firebase.firestore.Exclude;

import java.io.Serializable;

public class BillingList implements Serializable {
    @Exclude
    private String id;

   private String Dateoftask;
    private String title;
    private String description;
    private String debit_credit;
    private String fee_amount;
    private String referanceId;

    private String cumulative_amount;
    public BillingList()
    {

    }
    public BillingList(String id, String dateoftask, String title, String description, String debit_credit, String fee_amount, String cumulative_amount,String referanceId) {
        this.id = id;
        Dateoftask = dateoftask;
        this.title = title;
        this.description = description;
        this.debit_credit = debit_credit;
        this.fee_amount = fee_amount;
        this.cumulative_amount = cumulative_amount;
        this.referanceId = referanceId;
    }

    public String getReferanceId() {
        return referanceId;
    }

    public void setReferanceId(String referanceId) {
        this.referanceId = referanceId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDateoftask() {
        return Dateoftask;
    }

    public void setDateoftask(String dateoftask) {
        Dateoftask = dateoftask;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDebit_credit() {
        return debit_credit;
    }

    public void setDebit_credit(String debit_credit) {
        this.debit_credit = debit_credit;
    }

    public String getFee_amount() {
        return fee_amount;
    }

    public void setFee_amount(String fee_amount) {
        this.fee_amount = fee_amount;
    }

    public String getCumulative_amount() {
        return cumulative_amount;
    }

    public void setCumulative_amount(String cumulative_amount) {
        this.cumulative_amount = cumulative_amount;
    }
}
