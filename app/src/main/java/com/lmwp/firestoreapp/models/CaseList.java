package com.lmwp.firestoreapp.models;

import com.google.firebase.firestore.Exclude;

import java.io.Serializable;

public class CaseList  implements Serializable{

    String referenceId;
    String openedDate;
    String status;
    String closedDate;
    int cumulativeAmount ;

    @Exclude
    private String id;
    String idnumber;
 public CaseList(){

    }
    public CaseList(String referenceId, String openedDate, String status, String closedDate,String idnumber) {
        this.referenceId = referenceId;
        this.openedDate = openedDate;
        this.status = status;
        this.closedDate = closedDate;
        this.idnumber = idnumber;

    }

    public int getCumulativeAmount() {
        return cumulativeAmount;
    }

    public void setCumulativeAmount(int cumulativeAmount) {
        this.cumulativeAmount = cumulativeAmount;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    public String getOpenedDate() {
        return openedDate;
    }

    public void setOpenedDate(String openedDate) {
        this.openedDate = openedDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getClosedDate() {
        return closedDate;
    }

    public void setClosedDate(String closedDate) {
        this.closedDate = closedDate;
    }

    public String getIdnumber() {
        return idnumber;
    }

    public void setIdnumber(String idnumber) {
        this.idnumber = idnumber;
    }
}
