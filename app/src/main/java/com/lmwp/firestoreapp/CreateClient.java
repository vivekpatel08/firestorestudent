package com.lmwp.firestoreapp;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.lmwp.firestoreapp.utils.DatePickerFragment;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class CreateClient extends AppCompatActivity {


    private static final String TAG = "MainAc";

    private EditText fullnameEdt, contactNumber1Edt, contactNumber2Edt, physicalAddressEdt,idConsultantEdt,idEdtdateofbirthEdt;
    private Button idSaveClient;
    private String fullname, contactNumber1, contactNumber2, physicalAddress,idConsultant,idEdtdateofbirth;
    private FirebaseFirestore db;
    DatePickerDialog.OnDateSetListener ondate;
    ImageView bithDateImage,idEdtconsultantImage;
    boolean isDateofBirth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_client);

        getSupportActionBar().setTitle("Create Client");
        ActionBar actionBar = getSupportActionBar();

        // showing the back button in action bar
        actionBar.setDisplayHomeAsUpEnabled(true);

        // initializing our edittext and buttons
        fullnameEdt = findViewById(R.id.idEdtFullName);
        contactNumber1Edt = findViewById(R.id.idEdtContanct1);
        contactNumber2Edt = findViewById(R.id.idEdtContanct2);
        physicalAddressEdt = findViewById(R.id.idEdtPhysicalAddress);
        idConsultantEdt = findViewById(R.id.idEdtconsultant);
        idEdtdateofbirthEdt = findViewById(R.id.idEdtdateofbirth);
        bithDateImage = findViewById(R.id.idEdtDateofBirthImage);
        idEdtconsultantImage = findViewById(R.id.idEdtconsultantImage);


        idSaveClient = findViewById(R.id.idSaveClient);
        db = FirebaseFirestore.getInstance();
        // adding on click listener for button
        idSaveClient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // getting data from edittext fields.
                fullname = fullnameEdt.getText().toString();
                contactNumber1 = contactNumber1Edt.getText().toString();
                contactNumber2 = contactNumber2Edt.getText().toString();
                physicalAddress = physicalAddressEdt.getText().toString();
                idConsultant = idConsultantEdt.getText().toString();
                idEdtdateofbirth = idEdtdateofbirthEdt.getText().toString();

                if (TextUtils.isEmpty(fullname)) {
                    fullnameEdt.setError("Please enter title.");
                } else if (TextUtils.isEmpty(contactNumber1)) {
                    contactNumber1Edt.setError("Please enter contact number 1.");
                }  else if (TextUtils.isEmpty(physicalAddress)) {
                    physicalAddressEdt.setError("Please enter physical address");
                } else{
                   saveClientDetails();
                }
            }
        });


        idEdtconsultantImage.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("NewApi")
            @Override
            public void onClick(View v) {

                isDateofBirth=false;
                showDatePicker();

            }
        });
        bithDateImage.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("NewApi")
            @Override
            public void onClick(View v) {

                isDateofBirth=true;
                showDatePicker();

            }
        });

        ondate = new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
if(isDateofBirth)
{
    idEdtdateofbirthEdt.setText(String.valueOf(dayOfMonth) + "-" + String.valueOf(monthOfYear + 1)
            + "-" + String.valueOf(year));
}else {
    idConsultantEdt.setText(String.valueOf(dayOfMonth) + "-" + String.valueOf(monthOfYear + 1)
            + "-" + String.valueOf(year));
}

            }
        };

    }
    private void showDatePicker() {
        DatePickerFragment date = new DatePickerFragment();
        /**
         * Set Up Current Date Into dialog
         */
        Calendar calender = Calendar.getInstance();
        Bundle args = new Bundle();
        args.putInt("year", calender.get(Calendar.YEAR));
        args.putInt("month", calender.get(Calendar.MONTH));
        args.putInt("day", calender.get(Calendar.DAY_OF_MONTH));
        date.setArguments(args);
        /**
         * Set Call back to capture selected date
         */
        date.setCallBack(ondate);
        date.show( getSupportFragmentManager(),"Date Picker");
    }

    private void saveClientDetails() {

        Map<String, Object> clientData ;
        db = FirebaseFirestore.getInstance();

        clientData = new HashMap<>();
        Date dateData = Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat("mmddhhyyyymmss");
        String idnumber = dateFormat.format(dateData);
        clientData.put("fullname", fullname);
        clientData.put("idnumber", idnumber);
        clientData.put("dateofbirth", idEdtdateofbirth);
        clientData.put("contactnumber1", contactNumber1);
        clientData.put("contactnumber2", contactNumber2);
        clientData.put("physicaladdress", physicalAddress);
        clientData.put("firstconsultationdate", idConsultant);


        //user.put("createddate", Clock.systemUTC().instant());

        db.collection("clients")
                .add(clientData)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Toast.makeText(CreateClient.this, "Client details saved successfully.", Toast.LENGTH_SHORT).show();
                        onBackPressed();
                        Log.d(TAG, "DocumentSnapshot added with ID: " + documentReference.getId());
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error adding document", e);
                        Toast.makeText(CreateClient.this, "Failed to save details, please try later.", Toast.LENGTH_SHORT).show();
                    }
                });

    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}