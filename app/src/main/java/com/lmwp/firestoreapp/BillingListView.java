package com.lmwp.firestoreapp;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.lmwp.firestoreapp.adapters.BillingAdapter;
import com.lmwp.firestoreapp.adapters.ClientsAdapter;
import com.lmwp.firestoreapp.interfaces.viewClientDetails;
import com.lmwp.firestoreapp.models.BillingList;
import com.lmwp.firestoreapp.models.ClientList;
import com.lmwp.firestoreapp.utils.DataBridge;

import java.util.ArrayList;
import java.util.List;

public class BillingListView extends AppCompatActivity {

    FloatingActionButton mAddFab, mAddWorkFab, mAddClientFab;
    TextView addAlarmActionText, addPersonActionText;
    Boolean isAllFabsVisible;
    ListView billingListView;
    private FirebaseFirestore db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_billing_list);
        getSupportActionBar().setTitle("Billing List");
        ActionBar actionBar = getSupportActionBar();
        // showing the back button in action bar
        actionBar.setDisplayHomeAsUpEnabled(true);

        db = FirebaseFirestore.getInstance();
        mAddFab = findViewById(R.id.add_fab);
        billingListView = findViewById(R.id.billingList);

        mAddWorkFab = findViewById(R.id.add_alarm_fab);
        mAddClientFab = findViewById(R.id.add_person_fab);


        addAlarmActionText = findViewById(R.id.add_alarm_action_text);
        addPersonActionText = findViewById(R.id.add_person_action_text);
        mAddClientFab.setVisibility(View.GONE);
        mAddWorkFab.setVisibility(View.GONE);
        addAlarmActionText.setVisibility(View.GONE);
        addPersonActionText.setVisibility(View.GONE);

        isAllFabsVisible = false;
        mAddFab.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (!isAllFabsVisible) {

                           // mAddWorkFab.show();
                           mAddClientFab.show();
                         //  addAlarmActionText.setVisibility(View.VISIBLE);
                           addPersonActionText.setVisibility(View.VISIBLE);
                            isAllFabsVisible = true;
                        } else {


                           mAddClientFab.hide();
                            mAddWorkFab.hide();
                         //   addAlarmActionText.setVisibility(View.GONE);
                            addPersonActionText.setVisibility(View.GONE);


                            isAllFabsVisible = false;
                        }
                    }
                });


        mAddClientFab.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (!isAllFabsVisible) {

                            // mAddWorkFab.show();
                            mAddClientFab.show();
                            //  addAlarmActionText.setVisibility(View.VISIBLE);
                            addPersonActionText.setVisibility(View.VISIBLE);
                            isAllFabsVisible = true;
                        } else {


                            mAddClientFab.hide();
                            mAddWorkFab.hide();
                            //   addAlarmActionText.setVisibility(View.GONE);
                            addPersonActionText.setVisibility(View.GONE);


                            isAllFabsVisible = false;
                        }


                        Intent i =new Intent(BillingListView.this,CreateBilling.class);
                        startActivity(i);

                    }
                });




        GetBillingList();


    }

    private void GetBillingList() {

        db.collection("billing")
                .whereEqualTo("referanceId",DataBridge.selectedCaseItem.getReferenceId())
                .get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                ArrayList<BillingList> clientLists = new ArrayList<>();
                List<DocumentSnapshot> list = queryDocumentSnapshots.getDocuments();
                for (DocumentSnapshot d : list) {

                    Log.d("BillingList", "DocumentSnapshot added with ID: " + d.getData());
                    BillingList dataModal = d.toObject(BillingList.class);

                    clientLists.add(dataModal);
                }

                BillingAdapter adaptor = new BillingAdapter(BillingListView.this, clientLists);
                //set the view for the Drop down list
                //set the ArrayAdapter to the spinner
                billingListView.setAdapter(adaptor);
            }
        });
    }

    @Override
    protected void onResume() {
        GetBillingList();
        super.onResume();
    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}