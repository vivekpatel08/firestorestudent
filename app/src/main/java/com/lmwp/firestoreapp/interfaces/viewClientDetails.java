package com.lmwp.firestoreapp.interfaces;

import com.lmwp.firestoreapp.models.ClientList;

public interface viewClientDetails
{
        void checkDetails(ClientList clientdetails);
        void updateDetails(ClientList clientdetails);

}
