package com.lmwp.firestoreapp.interfaces;

import com.lmwp.firestoreapp.models.CaseList;

public interface AddDocumentInCase {

    void addDocument(String caseReferenceId,boolean isView);
    void addBilling(CaseList item);
    void changeStatus(CaseList item);


}
