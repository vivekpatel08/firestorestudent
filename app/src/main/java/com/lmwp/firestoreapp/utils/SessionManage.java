package com.lmwp.firestoreapp.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class SessionManage {

    Context _session_acti;
    SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "MyPrefs";
    public static final String name = "nameKey";
    public static final String pass = "passwordKey";
    public static final String EmployeeCode = "employeeCode";
    public static final String UserId = "Id";
    public static final String Position = "position";
    public static final String ZoneId = "zoneId";
    public static final String logoutKey = "logOutKey";


    public static boolean SESSION_FLAG = false;

    public SessionManage(Context _session_acti) {
        super();
        this._session_acti = _session_acti;
    }

    public void SessionStore(int Eid, String Ecode, String Eposition, String Ename, String zoneId) {
        sharedpreferences = _session_acti.getSharedPreferences(MyPREFERENCES,
                Context.MODE_PRIVATE);
        Editor editor = sharedpreferences.edit();
        editor.putString(name, Ename);
        editor.putString(EmployeeCode, Ecode);
        editor.putInt(UserId, Eid);
        editor.putString(Position, Eposition);
        editor.putString(ZoneId, zoneId);
        editor.commit();
    }


    public String PickValue(String Key) {
        String output = "";
        sharedpreferences = _session_acti.getSharedPreferences(MyPREFERENCES,
                Context.MODE_PRIVATE);
        output = sharedpreferences.getString(Key, null);
        return output;
    }

    public int PickEmpId(String Key) {
        int output = 0;
        sharedpreferences = _session_acti.getSharedPreferences(MyPREFERENCES,
                Context.MODE_PRIVATE);
        output = sharedpreferences.getInt(Key, 0);
        return output;
    }
    public void SaveValueInSession(String Key,String value) {
        sharedpreferences = _session_acti.getSharedPreferences(MyPREFERENCES,
                Context.MODE_PRIVATE);
        Editor editor = sharedpreferences.edit();
        editor.putString(Key, value);
        editor.commit();
    }
    public  void RemoveAllDataInSesssion()
    {
        SharedPreferences settings = _session_acti.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        settings.edit().clear().apply();
    }

}
