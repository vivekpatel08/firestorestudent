package com.lmwp.firestoreapp;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.lmwp.firestoreapp.utils.DataBridge;
import com.lmwp.firestoreapp.utils.DatePickerFragment;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class CreateCases extends AppCompatActivity {


    private static final String TAG = "MainAc";

    private EditText openDateEdt,  closeDateEdt;
    private Button submitCaseBtn;
    private String openDate, closeDate;
    private FirebaseFirestore db;
    ImageView openDateEdtImage,  closeDateEdtImage;
    Boolean isOpenDateSelected;
    DatePickerDialog.OnDateSetListener ondate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_case);

        getSupportActionBar().setTitle("Create Case");
        ActionBar actionBar = getSupportActionBar();

        // showing the back button in action bar
        actionBar.setDisplayHomeAsUpEnabled(true);

        // initializing our edittext and buttons
        openDateEdt = findViewById(R.id.idEdtOpenDate);

        closeDateEdt = findViewById(R.id.idEdtCloseDate);
        openDateEdtImage = findViewById(R.id.idEdtOpenDateImage);
        closeDateEdtImage = findViewById(R.id.idEdtCloseDateImage);
        submitCaseBtn = findViewById(R.id.idSaveCases);


        closeDateEdtImage.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("NewApi")
            @Override
            public void onClick(View v) {

                isOpenDateSelected=false;
                showDatePicker();

            }
        });
        openDateEdtImage.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("NewApi")
            @Override
            public void onClick(View v) {

                isOpenDateSelected=true;
                showDatePicker();

            }
        });

        db = FirebaseFirestore.getInstance();
        // adding on click listener for button
        submitCaseBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                openDate = openDateEdt.getText().toString();

                closeDate = closeDateEdt.getText().toString();





                if (TextUtils.isEmpty(openDate)) {
                    openDateEdt.setError("Please enter title");
                }  else if (TextUtils.isEmpty(closeDate)) {
                    closeDateEdt.setError("Please select due date.");
                }else{

               saveCaseLDetails();



                }
            }
        });


        ondate = new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                if(isOpenDateSelected)
                {
                    openDateEdt.setText(String.valueOf(dayOfMonth) + "-" + String.valueOf(monthOfYear + 1)
                            + "-" + String.valueOf(year));
                }else {
                    closeDateEdt.setText(String.valueOf(dayOfMonth) + "-" + String.valueOf(monthOfYear + 1)
                            + "-" + String.valueOf(year));
                }

            }
        };



    }

    private void showDatePicker() {
        DatePickerFragment date = new DatePickerFragment();
        /**
         * Set Up Current Date Into dialog
         */
        Calendar calender = Calendar.getInstance();
        Bundle args = new Bundle();
        args.putInt("year", calender.get(Calendar.YEAR));
        args.putInt("month", calender.get(Calendar.MONTH));
        args.putInt("day", calender.get(Calendar.DAY_OF_MONTH));
        date.setArguments(args);
        /**
         * Set Call back to capture selected date
         */
        date.setCallBack(ondate);
        date.show( getSupportFragmentManager(),"Date Picker");
    }

    private void saveCaseLDetails() {

        Map<String, Object> user ;
        db = FirebaseFirestore.getInstance();
        // Create a new user with a first and last name
        Date date = new Date();
        user = new HashMap<>();


        Date dateData = Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat("mmss");
        String referenceId = dateFormat.format(dateData);

        user.put("referenceId", "CA"+referenceId);
        user.put("openedDate", openDate);
        user.put("status", "In-Progress");
        user.put("closedDate", closeDate);
        user.put("idnumber", DataBridge.selectedClientDetails.getIdnumber());


        db.collection("cases")
                .add(user)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Toast.makeText(CreateCases.this, "Case details saved successfully.", Toast.LENGTH_SHORT).show();
                        onBackPressed();
                        Log.d(TAG, "DocumentSnapshot added with ID: " + documentReference.getId());
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error adding document", e);
                        Toast.makeText(CreateCases.this, "Failed to save details, please try later.", Toast.LENGTH_SHORT).show();
                    }
                });

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}