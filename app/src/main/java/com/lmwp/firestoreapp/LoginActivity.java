package com.lmwp.firestoreapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.lmwp.firestoreapp.models.UserList;
import com.lmwp.firestoreapp.utils.DataBridge;
import com.lmwp.firestoreapp.utils.SessionManage;

import java.util.ArrayList;
import java.util.List;

public class LoginActivity extends AppCompatActivity {
private EditText UsernameEdt, passwordEdt;
private String userName, password;
private FirebaseFirestore db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().hide();
       // ActionBar actionBar = getSupportActionBar();
        // showing the back button in action bar
        //actionBar.setDisplayHomeAsUpEnabled(true);
        SessionManage obj=new SessionManage(LoginActivity.this);


        if(obj.PickEmpId(SessionManage.UserId)!=0)
        {
            Intent i=new Intent(LoginActivity.this,WorkListActivity.class);
            startActivity(i);
        }


        db = FirebaseFirestore.getInstance();
        UsernameEdt= findViewById(R.id.idLoginEdtusername);
        passwordEdt=findViewById(R.id.idLoginEdtPassword);
        Button submitLogin =findViewById(R.id.idCheckLogin);
        Button idCheckRegister =findViewById(R.id.idCheckRegister);
        idCheckRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i =new Intent(LoginActivity.this,MainActivity.class);
                startActivity(i);
            }
        });

        submitLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userName= UsernameEdt.getText().toString();
                password=passwordEdt.getText().toString();
                if(TextUtils.isEmpty(userName))
                {
                    UsernameEdt.setError("Please enter username.");
                    return;
                }
                if(TextUtils.isEmpty(password))
                {
                    passwordEdt.setError("Please enter password.");
                    return;
                }


                db.collection("users")
                        .whereEqualTo("password", password)
                        .whereEqualTo("username", userName)
                        .get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {


                        List<UserList> userList = new ArrayList<>();
                        List<DocumentSnapshot> list = queryDocumentSnapshots.getDocuments();
                        for (DocumentSnapshot d : list) {

                            UserList dataModal = d.toObject(UserList.class);
                            dataModal.setId(d.getId());
                            userList.add(dataModal);

                            Log.e("LoginData"," "+dataModal.getId());
                        }
                        if(userList.isEmpty())
                        {
                            Toast.makeText(LoginActivity.this, "Please provide valid details.", Toast.LENGTH_SHORT).show();
                        return;
                        }
                        SessionManage obj=new SessionManage(LoginActivity.this);
                        obj.SessionStore(1,  "Ecode" ,  "Eposition",  "Ename",  "zoneId");
                        Intent i=new Intent(LoginActivity.this,WorkListActivity.class);
                        startActivity(i);


                    }
                });
            }
        });
    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finishAffinity();

        super.onBackPressed();
    }
}