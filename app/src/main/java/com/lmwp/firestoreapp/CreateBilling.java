package com.lmwp.firestoreapp;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.lmwp.firestoreapp.utils.DataBridge;
import com.lmwp.firestoreapp.utils.DatePickerFragment;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class CreateBilling extends AppCompatActivity {


    private static final String TAG = "MainAc";
    RadioButton credit, debit;
    private EditText EdtdateofTask, titleEdt, descriptionEdt, feeAmountEdt, cumulativeAmountEdt,EdtstartTime,EdtendTime,idEdtchargePerminuteEdt;
    ImageView EdtdateofTaskImage,EdtstartTimeImage,EdtendTimeImage;
    private Button idSaveBillingEdit;
    private String dateofTask, title, debit_credit="Credit", description, feeAmount, cumulativeAmount,chargeperminute,startTime,endTime;
    private FirebaseFirestore db;
    DatePickerDialog.OnDateSetListener ondate;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_billing);

        getSupportActionBar().setTitle("Create Billing");
        ActionBar actionBar = getSupportActionBar();

        // showing the back button in action bar
        actionBar.setDisplayHomeAsUpEnabled(true);

        // initializing our edittext and buttons
        EdtdateofTask = findViewById(R.id.idEdtdateofTask);
        EdtdateofTaskImage = findViewById(R.id.idEdtdateofTaskImage);
        EdtstartTime = findViewById(R.id.idEdtstartTime);
        EdtstartTimeImage = findViewById(R.id.idEdtStartTimeImage);
        EdtendTime = findViewById(R.id.idEdtendTime);
        EdtendTimeImage = findViewById(R.id.idEdtendTimeImage);
        titleEdt = findViewById(R.id.idEdttitle);
        idEdtchargePerminuteEdt = findViewById(R.id.idEdtchargePerminute);
        credit = findViewById(R.id.creditR);
        debit = findViewById(R.id.debitR);


        idEdtchargePerminuteEdt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(TextUtils.isEmpty(idEdtchargePerminuteEdt.getText().toString()))
                {
                    feeAmountEdt.setText("");
                }else{
                    calculateFeeAmount();
                }

            }
        });
        EdtstartTimeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(CreateBilling.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        EdtstartTime.setText( selectedHour + ":" + selectedMinute);
                        if(TextUtils.isEmpty(EdtendTime.getText().toString()) || TextUtils.isEmpty(idEdtchargePerminuteEdt.getText().toString()))
                        {
                            return;
                        }else{
                            calculateFeeAmount();
                        }


                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Start Time");
                mTimePicker.show();
            }
        });
        EdtendTimeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(CreateBilling.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        EdtendTime.setText( selectedHour + ":" + selectedMinute);
                        if(TextUtils.isEmpty(EdtstartTime.getText().toString()) || TextUtils.isEmpty(idEdtchargePerminuteEdt.getText().toString()))
                        {
                            return;
                        }else{
                            calculateFeeAmount();
                        }
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select End Time");
                mTimePicker.show();
            }
        });





        debit.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
        debit_credit="Debit";
    }
        });


        credit.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
        debit_credit="Credit";
    }
        });

        descriptionEdt = findViewById(R.id.idEdtDescription);
        feeAmountEdt = findViewById(R.id.idEdtfeeAmount);
        cumulativeAmountEdt = findViewById(R.id.idEdtcumulativeAmount);

        idSaveBillingEdit = findViewById(R.id.idSaveBilling);
        db = FirebaseFirestore.getInstance();
        // adding on click listener for button
        idSaveBillingEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // getting data from edittext fields.
                dateofTask = EdtdateofTask.getText().toString();
                title = titleEdt.getText().toString();

                description = descriptionEdt.getText().toString();
                feeAmount = feeAmountEdt.getText().toString();
                cumulativeAmount = cumulativeAmountEdt.getText().toString();
                startTime = EdtstartTime.getText().toString();
                endTime = EdtendTime.getText().toString();
                chargeperminute = idEdtchargePerminuteEdt.getText().toString();

                if (TextUtils.isEmpty(dateofTask)) {
                    EdtdateofTask.setError("Please enter date of task.");
                } else if (TextUtils.isEmpty(title)) {
                    titleEdt.setError("Please enter title.");
                }  else if (TextUtils.isEmpty(description)) {
                    descriptionEdt.setError("Please enter description.");
                } else{
                   saveClientDetails();
                }
            }
        });
        EdtdateofTaskImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePicker();
            }
        });
        ondate = new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {

                EdtdateofTask.setText(String.valueOf(dayOfMonth) + "-" + String.valueOf(monthOfYear + 1)
                        + "-" + String.valueOf(year));


            }
        };


    }
    private void showDatePicker() {
        DatePickerFragment date = new DatePickerFragment();

        /**
         * Set Up Current Date Into dialog
         */
        Calendar calender = Calendar.getInstance();
        Bundle args = new Bundle();
        args.putInt("year", calender.get(Calendar.YEAR));
        args.putInt("month", calender.get(Calendar.MONTH));
        args.putInt("day", calender.get(Calendar.DAY_OF_MONTH));
        date.setArguments(args);
        /**
         * Set Call back to capture selected date
         */
        date.setCallBack(ondate);
        date.show( getSupportFragmentManager(),"Date Picker");
    }

    private void saveClientDetails() {

        Map<String, Object> clientData ;
        db = FirebaseFirestore.getInstance();

        clientData = new HashMap<>();
        clientData.put("Dateoftask", dateofTask);
        clientData.put("title", title);
        clientData.put("description", description);
        clientData.put("debit_credit", debit_credit);
        clientData.put("fee_amount", feeAmount);
        clientData.put("startTime", startTime);
        clientData.put("endTime", endTime);
        clientData.put("chargeperminute", chargeperminute);
        clientData.put("referanceId", DataBridge.selectedCaseItem.getReferenceId());
        clientData.put("cumulative_amount", cumulativeAmount);

        db.collection("billing")
                .add(clientData)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Toast.makeText(CreateBilling.this, "Billing details saved successfully.", Toast.LENGTH_SHORT).show();
                        onBackPressed();
        //                Log.d(TAG, "DocumentSnapshot added with ID: " + documentReference.getId());
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error adding document", e);
                        Toast.makeText(CreateBilling.this, "Failed to save details, please try later.", Toast.LENGTH_SHORT).show();
                    }
                });

    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public long calculateFeeAmount()
    {
        // Dates to be parsed
        String time1 = EdtendTime.getText().toString();
        String time2 = EdtstartTime.getText().toString();

        // Creating a SimpleDateFormat object
        // to parse time in the format HH:MM:SS
        SimpleDateFormat simpleDateFormat
                = new SimpleDateFormat("HH:mm");

        // Parsing the Time Period
        Date date1 = null,date2=null;
        try {
            date1 = simpleDateFormat.parse(time1);

         date2 = simpleDateFormat.parse(time2);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        // Calculating the difference in milliseconds
        long differenceInMilliSeconds
                = Math.abs(date2.getTime() - date1.getTime());

        // Calculating the difference in Hours
        long differenceInHours
                = (differenceInMilliSeconds / (60 * 60 * 1000))
                % 24;

        // Calculating the difference in Minutes
        long differenceInMinutes
                = (differenceInMilliSeconds / (60 * 1000)) % 60;

        // Calculating the difference in Seconds
        long differenceInSeconds
                = (differenceInMilliSeconds / 1000) % 60;

        // Printing the answer
        System.out.println(
                "Difference is " + differenceInHours + " hours "
                        + differenceInMinutes + " minutes "
                        + differenceInSeconds + " Seconds. ");

        feeAmountEdt.setText(""+((differenceInHours*60)+differenceInMinutes)*Integer.parseInt(idEdtchargePerminuteEdt.getText().toString()));
return differenceInHours;
    }
}