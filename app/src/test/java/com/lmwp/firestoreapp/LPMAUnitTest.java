package com.lmwp.firestoreapp;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class LPMAUnitTest {

    @Test
    public void addition_isCorrect() {
        assertEquals(4, 2 + 2);

    }

    @Test
    public void  calculateCumulativeAmount() {
        float expectedProductPrice = 1.5f;
        int finalProductPrice=2;

        // When the product [basePrice] is 1, we expect the
        assertEquals(
                expectedProductPrice,
                finalProductPrice,
                0.001 // This is for number precision
        );
    }


}