package com.lmwp.firestoreapp;

import static org.junit.Assert.assertTrue;

import android.app.Activity;
import android.widget.Button;
import android.widget.EditText;

import junit.framework.AssertionFailedError;

public class LoginTest extends ActivityInstrumentationTestCase2<Activity> {

private static final String LAUNCHER_ACTIVITY_CLASSNAME = "com.lmwp.firestoreapp.view.activity.SplashActivity";
private static Class<?> launchActivityClass;
static {
try {
	launchActivityClass = Class.forName(LAUNCHER_ACTIVITY_CLASSNAME);
		} catch (ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
	}
	private ExtSolo solo;

	@SuppressWarnings("unchecked")
	public LoginTest() {
		super((Class<Activity>) launchActivityClass);
	}

	// executed before every test method
	
	public void setUp() throws Exception {
		//super.setUp();
		solo = new ExtSolo(getInstrumentation(), this.getClass()
				.getCanonicalName());
	}

	private Object getInstrumentation() {
		return null;
	}

	// executed after every test method
	
	public void tearDown()  {
		solo.finishOpenedActivities();
		solo.tearDown();
		super.tearDown();
	}

	public void testRecorded() throws Exception {
		try {
			assertTrue(
					"Wait for edit text (id: com.lmwp.firestoreapp.R.id.login_username_input) failed.",
					solo.waitForEditTextById(
							"com.lmwp.firestoreapp.R.id.login_username_input",
							20000));
			solo.enterText(
					(EditText) solo
							.findViewById("com.lmwp.firestoreapp.R.id.login_username_input"),
					"user1@gmail.com");
			solo.sendKey(ExtSolo.ENTER);
			solo.sleep(500);
			assertTrue(
					"Wait for edit text (id: com.lmwp.firestoreapp.R.id.login_password_input) failed.",
					solo.waitForEditTextById(
							"com.lmwp.firestoreapp.R.id.login_password_input",
							20000));
			solo.enterText(
					(EditText) solo
							.findViewById("com.lmwp.firestoreapp.R.id.login_password_input"),
					"123456");
			solo.sendKey(ExtSolo.ENTER);
			solo.sleep(500);
			assertTrue(
					"Wait for button (id: com.lmwp.firestoreapp.R.id.parse_login_button) failed.",
					solo.waitForButtonById(
							"com.lmwp.firestoreapp.R.id.parse_login_button",
							20000));
			solo.clickOnButton((Button) solo
                    .findViewById("com.lmwp.firestoreapp.R.id.parse_login_button"));
            assertTrue("Wait for text  list activity.",
                    solo.waitForActivity(LoginActivity.class));
			assertTrue("Wait for text RS.",
					solo.waitForText("KM", 20000));

			/*
				Custom class that enables proper clicking of ActionBar action items
			*/
            TestUtils.customClickOnView(solo, R.id.action_logout);

            solo.waitForDialogToOpen();
            solo.waitForText("OK");
            solo.clickOnText("OK");

            assertTrue("waiting for ParseLoginActivity after logout", solo.waitForActivity(LoginActivity.class));
            assertTrue(
                    "Wait for button (id: com.lmwp.firestoreapp.R.id.parse_login_button) failed.",
                    solo.waitForButtonById(
                            "com.lmwp.firestoreapp.R.id.parse_login_button",
                            20000));
		} catch (Exception e) {
			solo.fail(
					"com.example.android.apis.test.Test.testRecorded_scr_fail",
					e);
			throw e;
		}
	}
}